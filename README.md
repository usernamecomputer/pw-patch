Mirror Playwright CDN  
---
> Paths in commands are windows based.  
> Change `Scripts` to `bin` to correct paths for linux.  

# The project  

### The goal
To download the browsers (programs) that the Playwright content delivery network provides so a local mirror can be made.  
Keep the file paths the same so an un-patched Playwright installation can download the files (using [PLAYWRIGHT_DOWNLOAD_HOST][1] env variable).  
Create the program, so it doesn't need to be updated whenever a new browser revision is available.  
Download the browsers (programs) using the Playwright commandline interface tool.  

### How the goal is reached  
Using [playwright-core@1.35.0][2] a [patch][3] is created to do the following:  
- Mock the host platform  
- Redirect the path for the [browsers.json][4]  
- Redirect the download path for the zip files  

##### Mock the host platform  
Using the environment variable `PLAYWRIGHT_HOST_PLATFORM` the platform can be mocked.  

##### Redirect the path for the browsers.json  
Using the environment variable `PLAYWRIGHT_BROWSERS_JSON_FILE` the location of the [browsers.json][4] can be mocked.  

##### Redirect the download path for the zip files  
Using the environment variable `PLAYWRIGHT_BASE_DOWNLOAD_DIR` the location where the files and the filepaths are downloaded to is defined.  


# How to install  
> Preconditions  
> - Python >= 3.8 installed  
> - This project downloaded  

1. Open the commandline  
2. Navigate to the root of the downloaded project  
3. Create a virtual environment  
    ```bash
    python3 -m venv venv
    ```  
4. Activate the virtual environment  
   ```bash
   . ./venv/Scripts/activate
   ```  
6. Install the project  
   ```bash
   ./venv/Scripts/pip install .
   ```  

# How to use  
> Preconditions
> - Project installed
> 

1. Open the commandline
2. Navigate to the root of the downloaded project
3. Check project help text
   ```bash
   ./venv/Scripts/mirror --help
   ```  

#### Standard usage  
This downloads `chromium`, `firefox`, `webkit` and `ffmpeg`  
for platforms `win64` and `ubuntu22.04`  
to: `~/Downloads/ms-playwright`
```bash
./venv/Scripts/mirror
```  

#### Explicit usage  
Commandline flags with arguments can be given to the program to change the behaviour.  
The following can be specified:  
- `--download` for programs to download   
- `--platforms` which platforms to download for  
- `--outputdir` for where to place the download file & paths  
- `--browsersjson` for where the local [browsers.json][4] file is located  
```bash
./venv/Scripts/mirror --download firefox --platforms win64 --outputdir="C:/Users/<username>/Downloads/ms-playwright"  
```  
> Wrap the path in "quotes" if the `--outputdir` path contains spaces  

---

# How to contribute  
> Preconditions  
> - Python >= 3.8 installed  
> - This project forked to your repository  
> - The project cloned from your repository  

1. Open the commandline  
2. Navigate to the root of the downloaded project  
3. Create a virtual environment  
    ```bash
    python3 -m venv venv
    ```  
4. Activate the virtual environment  
   ```bash
   . ./venv/Scripts/activate
   ```  
5. Install the project  
   ```bash
   ./venv/Scripts/pip install --editable .[dev]
   ```  
   With option editable means PIP points to the local project folder.

---

## available platforms:
- generic-linux
- generic-linux-arm64
- ubuntu18.04
- ubuntu20.04
- ubuntu22.04
- ubuntu18.04-arm64
- ubuntu20.04-arm64
- ubuntu22.04-arm64
- debian11
- debian11-arm64
- mac10.13
- mac10.14
- mac10.15
- mac11
- mac11-arm64
- mac12
- mac12-arm64
- mac13
- mac13-arm64
- win64

## downloadable programs:
- chromium
- chromium-tip-of-tree
- chromium-with-symbols
- firefox
- firefox-beta
- webkit
- ffmpeg
- android



[1]: http://playwright.dev/docs/browsers#download-from-artifact-repository  
<!-- archive.today/QFCa4#download-from-artifact-repository   -->
[2]: https://www.npmjs.com/package/playwright-core/v/1.35.0
[3]: ./src/mirror/resources/playwright-core-1350.diff
[4]: https://github.com/microsoft/playwright/blob/release-1.35/packages/playwright-core/browsers.json