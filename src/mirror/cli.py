#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-
# cli.py

import argparse
from typing import Optional, Sequence

from mirror.const import DEFAULT_DOWNLOADS, DEFAULT_HOST_PLATFORMS, DEFAULT_BASE_DOWNLOAD_DIR


class CliArgs:
    downloadable: Sequence[str] = DEFAULT_DOWNLOADS
    host_platforms: Sequence[str] = DEFAULT_HOST_PLATFORMS
    download_dir: str = DEFAULT_BASE_DOWNLOAD_DIR
    browsers_json: Optional[str] = None
    verbose: bool = False

    def __repr__(self) -> str:
        return str({
            x: getattr(self, x)
            for x in ('downloadable', 'host_platforms', 'download_dir', 'browsers_json', 'verbose')
        })


def commandline_input(argv: Optional[Sequence[str]] = None) -> CliArgs:
    parser = argparse.ArgumentParser(
        prog="mirror",
        description="Download the browsers (programs) that the Playwright content delivery network provides",
    )
    parser.add_argument("-d", "--download",
                        nargs="*",
                        dest="downloadable",
                        help="programs that can be downloaded"
                        )
    parser.add_argument("-p", "--platforms",
                        nargs="*",
                        dest="host_platforms",
                        help="platforms that can use the programs"
                        )
    parser.add_argument("-o", "--outputdir",
                        dest="download_dir",
                        help="base output dir to where the download paths are placed",
                        )
    parser.add_argument("-b", "--browsersjson",
                        dest="browsers_json",
                        help="the browsers.json file of ",
                        )
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        help="output logs to stderr"
                        )
    return parser.parse_args(argv, namespace=CliArgs())


if __name__ == "__main__":
    if __debug__:
        import logging

        logging.basicConfig(level=logging.DEBUG)

    from pathlib import Path
    cmd = ['--download', 'chromium', 'firefox', 'ffmpeg', '--platforms', 'win64', 'generic-linux', '--outputdir=~/Downloads/out put']
    # cli = commandline_input(cmd)
    cli = commandline_input()
    print("--download: ", cli.downloadable)
    print("--platforms: ", cli.host_platforms)
    print("--outputdir: ", Path(cli.download_dir).expanduser().as_posix())
    print("--browsersjson: ", cli.browsers_json)
    print("--verbose: ", cli.verbose)
