#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-
# temppatch.py
# pip install patch-ng==1.17.4

from __future__ import annotations

import functools
from pathlib import Path
from typing import Optional, Union

import patch_ng as patch
from patch_ng import PatchSet

__all__ = ["TempPatch", "TempPatchError"]


class TempPatchError(Exception):
    pass


class TempPatch:
    _patch: PatchSet
    _is_patched: bool = False

    @property
    def patch(self) -> PatchSet:
        return self._patch

    def __init__(
        self, diff_file: Union[str, Path], strip: int = 0, root: Optional[Union[str, Path]] = None, fuzz: bool = False
    ) -> None:
        self._strip = strip
        self._root = root
        self._fuzz = fuzz

        _file = Path(diff_file)
        if not _file.exists() and not _file.is_file():
            raise ValueError(f"given file doesn't exist: [{str(_file)}]")
        self._patch = self._from_diff(_file)

    @functools.singledispatchmethod
    def _from_diff(self, diff: Union[str, bytes, Path]) -> PatchSet:
        raise NotImplementedError("not usable type in argument, type: [%s]" % type(diff))

    @_from_diff.register
    def _(self, diff: str):
        return self._from_diff(bytes(diff))

    @_from_diff.register
    def _(self, diff: bytes):
        patch_set = patch.fromstring(diff)  # type: Union[bool, PatchSet]
        if not bool(patch_set):
            raise TempPatchError("could not parse given diff")
        else:
            return patch_set

    @_from_diff.register
    def _(self, diff: Path):
        return self._from_diff(diff.read_bytes())

    def do(self) -> bool:
        if not bool(self._is_patched):
            success = self.patch.apply(
                strip=self._strip,
                root=self._root,
                fuzz=self._fuzz,
            )  # type: bool
            if not bool(success):
                raise TempPatchError("could not apply the patch")
            else:
                self._is_patched = not self._is_patched
                return success
        return True

    def undo(self, *args, **kwargs) -> bool:
        if bool(self._is_patched):
            success = self.patch.revert(
                strip=self._strip,
                root=self._root,
            )
            if not bool(success):
                raise TempPatchError("could not apply the patch")
            else:
                self._is_patched = not self._is_patched
                return success
        return True

    def __enter__(self) -> bool:
        return self.do()

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        return self.undo()


if __name__ == "__main__":
    if __debug__:
        import logging

        class PatchLogHandler(logging.Handler):
            def __init__(self):
                logging.Handler.__init__(self, logging.DEBUG)

            def emit(self, record):
                logstr = self.format(record)
                print(logstr)

        patchlog = logging.getLogger("patch")
        patchlog.handlers = []
        patchlog.addHandler(PatchLogHandler())

        logging.basicConfig(level=logging.DEBUG)
