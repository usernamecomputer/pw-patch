#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-
# __main__.py

import itertools
import logging
import os
import subprocess
from pathlib import Path
from importlib import resources as pkg_resources
from typing import Optional, Sequence

from playwright._impl._driver import compute_driver_executable, get_driver_env
import requests

from mirror.cli import CliArgs, commandline_input
from mirror.const import HOST_PLATFORM_ENV, BASE_DOWNLOAD_DIR_ENV, PLAYWRIGHT_BROWSERS_PATH_ENV, PLAYWRIGHT_BROWSERS_JSON_ENV, BROWSERS_JSON_URL, RESOURCES_DIR
from mirror.temppatch import TempPatch
from mirror.walkpath import walkpath


PKG_CORE_DIFF = (RESOURCES_DIR, "playwright-core-1350.diff")
if not pkg_resources.is_resource(*PKG_CORE_DIFF):
    pkg_path = str.join('.', map(str, PKG_CORE_DIFF))
    raise ImportError(f"couldn't find the resource file [{pkg_path}]")


def download(program: str) -> int:
    # copied from playwright.__main__:main
    driver_executable = compute_driver_executable()
    # ./venv/Script/playwright install <program>
    #  a.k.a.
    # <playwright package path>/driver/playwright.cmd install <program>
    #  which calls the node.exe that calls the playwright-core javascript cli.js
    process = subprocess.Popen(
        [str(driver_executable), "install", program],
        env=get_driver_env(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True  # decode bytes to string
    )
    with process.stdout as stdout:
        # remove the '\n' in the stdout text stream  and  log the stdout
        for line in map(str.rstrip, iter(stdout.readline, '')):
            logging.debug("PLAYWRIGHT CLI: %s", line)

    return process.wait()  # exit_code -> int


def set_environment(host_platform: str, download_dir: str, browsers_json_filepath: str) -> None:
    # un-patched (read: normally) playwright installs the programs.
    #  it installs it in:
    #  - windows %LOCALAPPDATA%\ms-playwright
    #  - linux ~/.cache/ms-playwright
    # the cli uses this directory as a cache
    # env. variable: 'PLAYWRIGHT_BROWSERS_PATH' can be set to change the installation directory
    os.environ[PLAYWRIGHT_BROWSERS_PATH_ENV] = Path(download_dir).expanduser().as_posix()
    logging.info("playwright browsers path set to: [%s]", os.environ[PLAYWRIGHT_BROWSERS_PATH_ENV])
    # set the path where the downloaded zips+paths are being downloaded
    # make sure the zip-path is user-expanded
    #  and the slashes should be posix style: `/`
    os.environ[BASE_DOWNLOAD_DIR_ENV] = Path(download_dir).expanduser().as_posix()  # PLAYWRIGHT_BASE_DOWNLOAD_DIR
    logging.info("downloading to: [%s]", os.environ[BASE_DOWNLOAD_DIR_ENV])

    # mock the host platform
    os.environ[HOST_PLATFORM_ENV] = str(host_platform).casefold()
    logging.info("current platform set to: [%s]", os.environ[HOST_PLATFORM_ENV])

    # set the path where the browsers.json file is
    os.environ[PLAYWRIGHT_BROWSERS_JSON_ENV] = Path(browsers_json_filepath).expanduser().as_posix()  # PLAYWRIGHT_BROWSERS_JSON_FILE
    logging.info("browser.json filepath set to: [%s]", os.environ[PLAYWRIGHT_BROWSERS_JSON_ENV])


def commandline_interface_entry(argv: Optional[Sequence[str]] = None) -> int:
    # capture the commandline arguments
    args: CliArgs = commandline_input(argv)
    if bool(args.verbose):
        logging.basicConfig(level=logging.DEBUG)

    # mock the browsers.json file or retrieve the latest
    if bool(args.browsers_json):
        browsers_json_filepath = args.browsers_json
    else:
        logging.info('downloading browser.json from: [%s]', BROWSERS_JSON_URL)
        # download the latest browsers.json file  and  place the content in a temporary file
        response = requests.get(BROWSERS_JSON_URL)
        # create the filepath
        browsers_json_filepath = Path(args.download_dir).expanduser().joinpath('browsers.json')
        # assure the directories to the args.download_dir are created
        browsers_json_filepath.parent.mkdir(parents=True, exist_ok=True)
        # write the downloaded content to the file
        browsers_json_filepath.write_bytes(response.content)
        # make the variable a string, with the correct slashes, just like given cli-argument

    # site-packages/playwright/driver/playwright.cmd -> site-packages/playwright/driver/package
    playwright_core_dir = compute_driver_executable().parent.joinpath("package")
    with pkg_resources.path(*PKG_CORE_DIFF) as pkg:
        # patch and un-patch the diff with TempPatch
        #  may need to change strip (?)  strip removes leading path parts from the diff
        with TempPatch(pkg, strip=2, root=playwright_core_dir, fuzz=True):
            # loop over the product-combination of the host platforms and downloadable programs
            for host_platform, program in itertools.product(args.host_platforms, args.downloadable):
                set_environment(host_platform, args.download_dir, browsers_json_filepath)
                try:
                    logging.info("starting download, program:[%s] on platform: [%s]", program, host_platform)
                    # download the program
                    download(program)
                except Exception as err:
                    logging.error("error happened in main during download of [%s] on [%s]", program, host_platform)
                    logging.exception(err, exc_info=True)
                finally:
                    logging.info("download success, program: [%s] on platform: [%s]", program, host_platform)

    if not bool(args.browsers_json):
        # browsers.json is downloaded, remove it
        try:
            Path(browsers_json_filepath).expanduser().unlink(missing_ok=True)
        except Exception as err:
            logging.error("can't remove file: [%s]", str(browsers_json_filepath))
            logging.exception(err, exc_info=True)

    links_dir = Path(args.download_dir).expanduser().joinpath('.links')
    try:
        for file in walkpath(links_dir):
            file.unlink(missing_ok=True)
        links_dir.rmdir()
    except Exception as err:
        logging.error("couldn't remove file(s) in, or the directory: [%s]", str(links_dir))
        logging.exception(err, exc_info=True)

    return 0


if __name__ == "__main__":
    if __debug__:
        import logging
        logging.basicConfig(level=logging.DEBUG)

    from src.mirror.const import DEFAULT_DOWNLOADS, DEFAULT_BASE_DOWNLOAD_DIR
    from src.mirror.util.browser_json_generator import get_release_revisions, create_browser_json

    download_dir = Path(DEFAULT_BASE_DOWNLOAD_DIR).expanduser()
    download_dir.mkdir(parents=True, exist_ok=True)
    browser_json_file = download_dir.joinpath('browser.json')

    for program, revisions in get_release_revisions(range(30, (36 + 1))).items():
        if program not in DEFAULT_DOWNLOADS:
            continue
        for revision in revisions:
            print(f"{program = } | {revision = }")
            create_browser_json(program, revision, browser_json_file)
            cli = [
                '--verbose',
                '--download', program,
                '--browsersjson', browser_json_file.as_posix(),
                '--outputdir', download_dir.as_posix()
            ]
            print(str.join(' ', cli))

            commandline_interface_entry(cli)

    # raise SystemExit()
