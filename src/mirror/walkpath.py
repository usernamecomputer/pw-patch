#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# walkpath.py

from typing import Union, Iterator, Optional
from pathlib import Path
from sys import getrecursionlimit


def walkpath(
    path: Union[str, Path],
    max_depth: Optional[int] = None,
    _current_depth: int = 0
) -> Iterator[Path]:
    # set boundaries
    if not bool(max_depth) or max_depth < 0:
        max_depth = getrecursionlimit() - 1

    _path = Path(path)
    # yield file
    if _path.is_file():
        yield _path
    # test if boundaries have been reached
    elif _path.is_dir() and _current_depth <= max_depth:
        try:
            for _dir_item in _path.iterdir():
                yield from walkpath(
                    _dir_item, max_depth, (_current_depth + 1)
                )
        except PermissionError:
            pass
