#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-
# const.py

from pathlib import Path


def _import_path(seq) -> str:
    return str.join(".", map(str, seq))


RESOURCES_DIR = _import_path(("mirror", "resources",))

# urls
BROWSERS_JSON_URL = r"https://raw.githubusercontent.com/microsoft/playwright/main/packages/playwright-core/browsers.json"

# ENVironment variables
HOST_PLATFORM_ENV = "PLAYWRIGHT_HOST_PLATFORM"
BASE_DOWNLOAD_DIR_ENV = "PLAYWRIGHT_BASE_DOWNLOAD_DIR"
PLAYWRIGHT_BROWSERS_PATH_ENV = "PLAYWRIGHT_BROWSERS_PATH"
PLAYWRIGHT_BROWSERS_JSON_ENV = "PLAYWRIGHT_BROWSERS_JSON_FILE"

# default download values
DEFAULT_HOST_PLATFORMS = ("win64", "ubuntu22.04")
DEFAULT_DOWNLOADS = ("ffmpeg", "firefox", "chromium", "webkit")  # same as cli-command: `playwright install`
DEFAULT_BASE_DOWNLOAD_DIR = "~/Downloads/ms-playwright"
